package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.*;

public class DuplicateFinder {

    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null)
            throw new IllegalArgumentException();

        List<String> notCounted = new ArrayList<>();
        if (sourceFile.exists() && !sourceFile.isDirectory() && sourceFile.canRead()) {
            try (FileReader fr = new FileReader(sourceFile);
                 BufferedReader br = new BufferedReader(fr)) {
                String line;
                while ((line = br.readLine()) != null) {
                    notCounted.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else
            return false;

        Map<String, Integer> counted = new TreeMap<>();
        for (String r : notCounted) {
            counted.put(r, Collections.frequency(notCounted, r));
        }

        try {
            FileWriter output = new FileWriter(targetFile);
            BufferedWriter bufferWritter = new BufferedWriter(output);
            try (PrintWriter out = new PrintWriter(bufferWritter)) {
                for (Map.Entry<String, Integer> iter : counted.entrySet()) {
                    out.println(iter.getKey() + " [" + iter.getValue() + "]");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
