package com.tsystems.javaschool.tasks.calculator;

public class Calculator {
    private String operation;
    private int position = -1;
    private int character;

    void nextChar() {
        character = ++position < operation.length()
                ? operation.charAt(position)
                : -1;
    }

    private boolean compareNextCharacter(int characterToCompare) {
        while (character == ' ')
            nextChar();

        if (character == characterToCompare) {
            nextChar();
            return true;
        }

        return false;
    }

    public String evaluate(String operation) {
        if (operation == null || operation.isEmpty())
            return null;

        this.operation = operation;
        try {
            nextChar();
            double result = parseExpression();
            int i = (int) result;
            return result == i ? String.valueOf(i) : String.valueOf(result);
        } catch (RuntimeException t) {
            return null;
        }
    }

    private double parseExpression() {
        double result = parseTerm();
        for (; ; ) {
            if (compareNextCharacter('+'))
                result = addition(result, parseTerm());
            else if (compareNextCharacter('-'))
                result = substraction(result, parseTerm());
            else
                return result;
        }
    }

    private double parseTerm() {
        double result = parseFactor();
        for (; ; ) {
            if (compareNextCharacter('*'))
                result = multiplication(result, parseFactor());
            else if (compareNextCharacter('/'))
                result = division(result, parseFactor());
            else
                return result;
        }
    }

    private double parseFactor() {
        double x;
        int startPos = this.position;
        if (compareNextCharacter('(')) {
            x = parseExpression();
            if (!compareNextCharacter(')'))
                throw new RuntimeException("Bracket is not closed");
        } else if ((character >= '0' && character <= '9') ||
                character == '.' ||
                character == ',') {
            while ((character >= '0' && character <= '9') ||
                    character == '.' ||
                    character == ',')
                nextChar();

            x = Double.parseDouble(operation.substring(startPos, this.position));
        } else {
            throw new RuntimeException("Unexpected: " + (char) character);
        }

        return x;
    }

    public double addition(double fistOperand, double secondOperand) {
        return fistOperand + secondOperand;
    }

    public double substraction(double fistOperand, double secondOperand) {
        return fistOperand - secondOperand;
    }

    public double division(double fistOperand, double secondOperand) {
        if (secondOperand == 0)
            throw new ArithmeticException("Divide by zero.");

        return fistOperand / secondOperand;
    }

    public double multiplication(double fistOperand, double secondOperand) {
        return fistOperand * secondOperand;
    }
}
