package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("One of arguments is null");
        }

        Iterator yListIterator = y.iterator();
        for (Object xObject : x) {
            if (yListIterator.hasNext()) {
                while (yListIterator.hasNext()) {
                    if (xObject.equals(yListIterator.next()))
                        break;
                }
            } else {
                return false;
            }
        }

        return true;
    }
}
